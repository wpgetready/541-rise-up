﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyPlayer : MonoBehaviour {

    public SpriteRenderer icon;
    public Sprite[] iconSprites;
    public GameObject playerIconPrefab;
    public GameObject bubble;
    public GameObject[] bubbleParts;
    public float velMov = 1;

    private Rigidbody2D myBody;
    private float boundaryLeft, boundaryRight;

    private void Awake()
    {
        if (PlayerPrefs.HasKey("SensitivityMovement")) velMov = PlayerPrefs.GetFloat("SensitivityMovement");

        myBody = GetComponent<Rigidbody2D>();

        Sprite spriteBubble = bubble.GetComponent<SpriteRenderer>().sprite;
        float widthSprite = spriteBubble.texture.width / spriteBubble.pixelsPerUnit;
        float heightUnity = Camera.main.orthographicSize * 2;
        float UnitsWidth = Screen.width * heightUnity / Screen.height;

        boundaryLeft = UnitsWidth / -2 + widthSprite / 2;
        boundaryRight = UnitsWidth / 2 - widthSprite / 2;
    }

    void Start()
    {
        UpdateSprite();
    }

    void Update()
    {
#if UNITY_EDITOR
        float X = Mathf.Clamp(transform.position.x + Input.GetAxisRaw("Horizontal") * velMov * Time.deltaTime, boundaryLeft, boundaryRight);
#else
        float X = Mathf.Clamp(transform.position.x + Input.acceleration.x * velMov * Time.deltaTime, boundaryLeft, boundaryRight);
#endif
        Vector3 newPos = new Vector3(X, transform.position.y, transform.position.z);
        myBody.MovePosition(newPos);
    }

    public void UpdateSprite()
    {
        int selectedType = CUtils.GetPlayerType();
        icon.sprite = iconSprites[selectedType];
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Obstacle" && MainController.IsPlaying())
        {
            MainController.instance.CollideWithObstacle();
            MainController.instance.CheckAndShowContinue();
            icon.gameObject.SetActive(false);
            GetComponent<CircleCollider2D>().isTrigger = true;
            bubble.SetActive(false);
            foreach (GameObject p in bubbleParts)
            {
                p.SetActive(true);
            }
            GetComponent<Animator>().SetTrigger("Break");
            Sound.instance.Play(Sound.Others.Break);
            Timer.Schedule(this, 0.5f, () =>
            {
                Sound.instance.Play(Sound.Others.Die);
            });
            SpawnIcon();
        }
    }    

    private void SpawnIcon()
    {
        GameObject obj = (GameObject)Instantiate(playerIconPrefab);
        obj.transform.localPosition = icon.transform.position;
        obj.GetComponent< SpriteRenderer>().sprite = iconSprites[CUtils.GetPlayerType()];
        obj.transform.localScale = Vector3.one * 0.5f;
    }

    public void Reset()
    {
        icon.gameObject.SetActive(true);
        GetComponent<CircleCollider2D>().isTrigger = false;
        bubble.SetActive(true);
        foreach(GameObject p in bubbleParts)
        {
            p.SetActive(false);
        }

        transform.position = new Vector2(0f, -3);
    }

    public void ModifySensitivity()
    {
        velMov = MainController.instance.sliderSensivility.value;
        PlayerPrefs.SetFloat("SensitivityMovement", velMov);
    }
}
